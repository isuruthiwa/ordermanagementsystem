package com.nCinga.dao;

import com.nCinga.bo.Buyer;

import java.io.*;
import java.util.LinkedList;

public class BuyersDao {

    private static BuyersDao buyersDao;
    private LinkedList<Buyer> buyers;

    private BuyersDao() throws IOException {
        buyers=new LinkedList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("buyers.csv")))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] values = line.split(",");
                int buyerId = Integer.parseInt(values[0]);
                String buyerName = values[1];

                buyers.add(new Buyer(buyerId,buyerName));
            }
        }
    }

    public static BuyersDao getInstance() throws IOException {
        buyersDao= buyersDao==null ? new BuyersDao():buyersDao;
        return buyersDao;
    }

    public void addNewBuyer(String buyerName) throws IOException {
        buyers.add(new Buyer(buyerName));
        String buyerString= buyers.getLast().getBuyerId()+","+buyerName+"\r";
        appendToFile(new File("buyers.csv"),buyerString);
    }

    private static void appendToFile(File file, String appendString) throws IOException {
        FileWriter fileWriter = null;
        BufferedWriter bufferedWriter = null;
        try {
            fileWriter = new FileWriter(file,true);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.append(appendString);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(bufferedWriter!=null)
                    bufferedWriter.close();
            }
            finally {
                if(fileWriter!=null)
                    fileWriter.close();
            }
        }
    }

    public boolean removeBuyerById(int buyerId){
        for (int index = 0; index <buyers.size() ; index++) {
            if (buyers.get(index).getBuyerId() == buyerId){
                buyers.remove(index);
                System.out.println(buyers);
                return true;
            }
        }
        return false;
    }

    public Buyer findBuyerById(int buyerId){
        for (Buyer buyer : buyers) {
            if (buyer.getBuyerId() == buyerId) {
                return buyer;
            }
        }
        return null;
    }
}
