package com.nCinga.dao;

import com.nCinga.bo.Order;
import com.nCinga.bo.Product;
import javafx.util.Pair;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.IntStream;

public class OrdersDao {
    private static OrdersDao ordersDao;
    private LinkedList<Order> orders;

    private OrdersDao() throws IOException {
        orders= new LinkedList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("orders.csv"))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] values = line.split(",");
                int orderId = Integer.parseInt(values[0]);
                int buyerId = Integer.parseInt(values[1]);
                int productId = Integer.parseInt(values[2]);
                int orderedQuantity = Integer.parseInt(values[3]);
                Date orderedDate = new SimpleDateFormat("dd/MM/yyyy").parse(values[4]);

                orders.add(new Order(orderId,buyerId,productId,orderedQuantity,orderedDate));
            }
        } catch (ParseException e) {
            System.out.println("Invalid Date");;
        }
    }

    public static OrdersDao getInstance() throws IOException {
        ordersDao= ordersDao==null? new OrdersDao():ordersDao;
        return ordersDao;
    }

    public void addNewOrder(int buyerId, int productId, int orderedQuantity, String orderedDate) throws ParseException, IOException {
        Date formattedDate= new SimpleDateFormat("dd/MM/yyyy").parse(orderedDate);
        orders.add(new Order(buyerId,productId,orderedQuantity,formattedDate));
        String orderString= orders.getLast().getOrderId()+","+buyerId+","+productId+","+orderedQuantity+","+orderedDate+"\r";
        appendToFile(new File("orders.csv"),orderString);
    }

    private static void appendToFile(File file, String appendString) throws IOException {
        FileWriter fileWriter = null;
        BufferedWriter bufferedWriter = null;
        try {
            fileWriter = new FileWriter(file,true);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.append(appendString);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(bufferedWriter!=null)
                    bufferedWriter.close();
            }
            finally {
                if(fileWriter!=null)
                    fileWriter.close();
            }
        }
    }


    public boolean removeOrderById(int orderId){
        for (int index = 0; index <orders.size() ; index++) {
            if (orders.get(index).getOrderId() == orderId){
                orders.remove(index);
                System.out.println(orders);
                return true;
            }
        }
        return false;
    }

    public Order findOrderById(int orderId){
        for (Order order : orders) {
            if (order.getOrderId() == orderId) {
                return order;
            }
        }
        return null;
    }

    public ArrayList<Order> findOrderForBuyer(int buyerId){
        ArrayList<Order> ordersForBuyer=new ArrayList<>();
        for(Order order: orders){
            if(order.getBuyerId() == buyerId)
                ordersForBuyer.add(order);
        }
        return ordersForBuyer;
    }

    public int findOrderForSeller(int productId){
        int orderCount=0;
        for(Order order: orders){
            if(order.getProductId() == productId)
                orderCount+=order.getOrderedQuantity();
        }
        return orderCount;
    }
}
