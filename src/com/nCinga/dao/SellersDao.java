package com.nCinga.dao;

import com.nCinga.bo.Seller;

import java.io.*;
import java.util.LinkedList;

public class SellersDao {
    private static SellersDao sellersDao;
    private LinkedList<Seller> sellers;

    private SellersDao() throws IOException {
        sellers= new LinkedList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("buyers.csv"))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] values = line.split(",");
                int sellerId = Integer.parseInt(values[0]);
                String sellerName = values[1];

                sellers.add(new Seller(sellerId,sellerName));
            }
        }
    }

    public static SellersDao getInstance() throws IOException {
        sellersDao = sellersDao==null ? new SellersDao(): sellersDao;
        return sellersDao;
    }

    public void addNewSeller(String sellerName) throws IOException {
        sellers.add(new Seller(sellerName));
        String sellerString= sellers.getLast().getSellerId()+","+sellerName+"\r";
        appendToFile(new File("sellers.csv"),sellerString);
    }

    private static void appendToFile(File file, String appendString) throws IOException {
        FileWriter fileWriter = null;
        BufferedWriter bufferedWriter = null;
        try {
            fileWriter = new FileWriter(file,true);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.append(appendString);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(bufferedWriter!=null)
                    bufferedWriter.close();
            }
            finally {
                if(fileWriter!=null)
                    fileWriter.close();
            }
        }
    }

    public boolean removeSellerById(int sellerId){
        for (int index = 0; index <sellers.size() ; index++) {
            if (sellers.get(index).getSellerId() == sellerId){
                sellers.remove(index);
                System.out.println(sellers);
                return true;
            }
        }
        return false;
    }

    public Seller findSellerById(int sellerId){
        for (Seller seller : sellers) {
            if (seller.getSellerId() == sellerId) {
                return seller;
            }
        }
        return null;
    }
}
