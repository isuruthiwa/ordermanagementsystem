package com.nCinga.dao;

import com.nCinga.bo.Product;
import sun.awt.image.ImageWatched;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.function.Consumer;

public class ProductsDao {

    private static ProductsDao productsDao;
    private LinkedList<Product> products;

    private ProductsDao() throws IOException {
        products = new LinkedList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("products.csv"))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] values = line.split(",");
                int productId = Integer.parseInt(values[0]);
                int sellerId = Integer.parseInt(values[1]);
                String productName = values[2];
                int productPrice = Integer.parseInt(values[3]);
                int availableQuantity = Integer.parseInt(values[4]);

                products.add(new Product(productId,sellerId,productName,productPrice,availableQuantity));
            }
        }
    }

    public static ProductsDao getInstance() throws IOException {
        productsDao= productsDao==null ? new ProductsDao(): productsDao;
        return productsDao;
    }

    public void addNewProduct(int sellerId,String productName, int productPrice, int availableQuantity) throws IOException {
        products.add(new Product(sellerId,productName,productPrice,availableQuantity));
        String productString= products.getLast().getProductId()+","+sellerId+productName+productPrice+availableQuantity+"\r";
        appendToFile(new File("products.csv"),productString);
    }

    private static void appendToFile(File file, String appendString) throws IOException {
        FileWriter fileWriter = null;
        BufferedWriter bufferedWriter = null;
        try {
            fileWriter = new FileWriter(file,true);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.append(appendString);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if(bufferedWriter!=null)
                    bufferedWriter.close();
            }
            finally {
                if(fileWriter!=null)
                    fileWriter.close();
            }
        }
    }

    public boolean removeProductById(int productId){
        for (int index = 0; index <products.size() ; index++) {
            if (products.get(index).getProductId() == productId){
                products.remove(index);
                System.out.println(products);
                return true;
            }
        }
        return false;
    }

    public Product findProductById(int productId){
        for (Product product : products) {
            if (product.getProductId() == productId) {
                return product;
            }
        }
        return null;
    }

    public LinkedList<Product> findProductsBySellerId(int sellerId){
        LinkedList<Product> productsBySeller= new LinkedList<>();
        for (Product product : products) {
            if (product.getSellerId() == sellerId) {
                productsBySeller.add(product);
            }
        }
        return productsBySeller;
    }

    public LinkedList<Product> getProducts(){
        return products;
    }

    public boolean orderProduct(int productId, int orderQuantity){
        for(Product product:products){
            if(product.getProductId()==productId){
                if(product.getAvailableQuantity()>=orderQuantity){
                    product.decreaseAvailableQuantity(orderQuantity);
                    return true;
                }
                else
                    return false;
            }
        }
        return false;
    }
}
