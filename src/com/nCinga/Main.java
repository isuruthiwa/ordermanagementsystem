package com.nCinga;

import com.nCinga.dao.BuyersDao;
import com.nCinga.dao.OrdersDao;
import com.nCinga.service.impl.BuyerServiceImpl;
import com.nCinga.service.impl.OrderServiceImpl;
import com.nCinga.service.impl.ProductServiceImpl;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        OrderServiceImpl orderService =OrderServiceImpl.getInstance();
        ProductServiceImpl productService= ProductServiceImpl.getInstance();
        BuyerServiceImpl buyerService = BuyerServiceImpl.getInstance();

        Scanner scanner=new Scanner(System.in);
        while (true) {
            System.out.println(":: Order Management System ::\n");

            System.out.println("1. View Products" +
                    "\n2. Order a Product" +
                    "\n3. View Orders" +
                    "\n4. View Orders for Seller" +
                    "\n5. View Products from Seller" +
                    "\n0. Exit\n");
            System.out.print("Enter your choice: ");

            int input = scanner.nextInt();
            if (input == 1) {
                System.out.println(productService.getProducts());
            } else if (input == 2) {
                System.out.print("Enter your BuyerId: ");
                int buyerId = scanner.nextInt();
                System.out.print("Enter the product: ");
                int productId = scanner.nextInt();
                System.out.print("Enter the quantity: ");
                int quantity = scanner.nextInt();
                System.out.println(buyerService.placeAnOrder(productId, quantity, buyerId));
            } else if (input == 3) {
                System.out.print("Enter your BuyerId: ");
                int buyerId = scanner.nextInt();
                System.out.println(orderService.getOrdersForBuyer(buyerId));
            } else if (input == 4) {
                System.out.print("Enter the Seller ID: ");
                int sellerID = scanner.nextInt();
                System.out.println(orderService.getOrdersForSeller(sellerID));
            } else if (input == 5){
                System.out.print("Enter the Seller ID: ");
                int sellerID = scanner.nextInt();
                System.out.println(productService.productsFromSeller(sellerID));
            } else if(input==0)
                break;
            else
                System.out.println("Invalid input");

            System.out.println();
        }
    }
}
