package com.nCinga.bo;

import java.util.Date;

public class Order {
    private static int orderCount=0;
    private int orderId;
    private int buyerId;
    private int productId;
    private int orderedQuantity;
    private Date orderedDate;

    public Order(int buyerId, int productId, int orderedQuantity, Date orderedDate){
        setBuyerId(buyerId);
        setProductId(productId);
        setOrderedQuantity(orderedQuantity);
        setOrderedDate(orderedDate);
        setOrderId(++orderCount);
    }

    public Order(int orderId, int buyerId, int productId, int orderedQuantity, Date orderedDate){
        setBuyerId(buyerId);
        setProductId(productId);
        setOrderedQuantity(orderedQuantity);
        setOrderedDate(orderedDate);
        setOrderId(orderId);
        orderCount=orderId;
    }

    public int getOrderId() {
        return orderId;
    }

    private void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getBuyerId() {
        return buyerId;
    }

    private void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    public int getProductId() {
        return productId;
    }

    private void setProductId(int productId) {
        this.productId = productId;
    }

    public int getOrderedQuantity() {
        return orderedQuantity;
    }

    private void setOrderedQuantity(int orderedQuantity) {
        this.orderedQuantity = orderedQuantity;
    }

    public Date getOrderedDate() {
        return orderedDate;
    }

    private void setOrderedDate(Date orderedDate) {
        this.orderedDate = orderedDate;
    }


    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", buyerId=" + buyerId +
                ", productId=" + productId +
                ", orderedQuantity=" + orderedQuantity +
                ", orderedDate=" + orderedDate +
                '}';
    }
}
