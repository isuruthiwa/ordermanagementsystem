package com.nCinga.bo;

public class Seller {
    private static int sellerCount=0;
    private int sellerId;
    private String sellerName;

    public Seller(String name){
        setSellerName(name);
        setSellerId(++sellerCount);
    }

    public Seller(int sellerId,String name){
        setSellerName(name);
        setSellerId(sellerId);
        sellerCount=sellerId;
    }

    public int getSellerId() {
        return sellerId;
    }

    private void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    private void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    @Override
    public String toString() {
        return "Seller{" +
                "sellerId=" + sellerId +
                ", sellerName='" + sellerName + '\'' +
                '}';
    }
}
