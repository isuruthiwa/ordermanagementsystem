package com.nCinga.bo;

public class Buyer {
    private static int buyerCount=0;
    private int buyerId;
    private String buyerName;

    public Buyer(String name){
        setBuyerName(name);
        setBuyerId(++buyerCount);
    }

    public Buyer(int buyerId,String name){
        setBuyerName(name);
        setBuyerId(buyerId);
        buyerCount=buyerId;
    }

    public int getBuyerId() {
        return buyerId;
    }

    private void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerName() {
        return buyerName;
    }

    private void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    @Override
    public String toString() {
        return "Buyer{" +
                "buyerId=" + buyerId +
                ", buyerName='" + buyerName + '\'' +
                '}';
    }
}
