package com.nCinga.bo;

public class Product {
    private static int productCount =0;
    private int productId;
    private int sellerId;
    private String productName;
    private int productPrice;
    private int availableQuantity;

    public Product(int sellerId, String productName, int productPrice, int availableQuantity){
        setSellerId(sellerId);
        setProductName(productName);
        setProductPrice(productPrice);
        setAvailableQuantity(availableQuantity);
        setProductId(++productCount);
    }

    public Product(int productId,int sellerId, String productName, int productPrice, int availableQuantity){
        setSellerId(sellerId);
        setProductName(productName);
        setProductPrice(productPrice);
        setAvailableQuantity(availableQuantity);
        setProductId(productId);
        productCount=productId;
    }

    public int getProductId() {
        return productId;
    }

    private void setProductId(int productId) {
        this.productId = productId;
    }

    public int getSellerId() {
        return sellerId;
    }

    private void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getProductName() {
        return productName;
    }

    private void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductPrice() {
        return productPrice;
    }

    private void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }

    public int getAvailableQuantity() {
        return availableQuantity;
    }

    private void setAvailableQuantity(int availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public void decreaseAvailableQuantity(int orderQuantity){
        setAvailableQuantity(availableQuantity-orderQuantity);
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", sellerId=" + sellerId +
                ", productName='" + productName + '\'' +
                ", productPrice=" + productPrice +
                ", availableQuantity=" + availableQuantity +
                '}';
    }
}
