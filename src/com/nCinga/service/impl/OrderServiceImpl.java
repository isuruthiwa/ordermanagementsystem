package com.nCinga.service.impl;

import com.nCinga.bo.Order;
import com.nCinga.bo.Product;
import com.nCinga.dao.OrdersDao;
import com.nCinga.dao.ProductsDao;
import com.nCinga.service.OrderService;
import com.sun.org.apache.xpath.internal.operations.Or;

import java.io.IOException;
import java.util.*;
import java.util.stream.IntStream;

public class OrderServiceImpl implements OrderService {
    private OrdersDao ordersDao;
    private ProductsDao productsDao;

    private static OrderServiceImpl orderServiceImpl;

    private OrderServiceImpl() throws IOException {
        ordersDao = OrdersDao.getInstance();
        productsDao= ProductsDao.getInstance();
    }

    public static OrderServiceImpl getInstance() throws IOException {
        orderServiceImpl = orderServiceImpl==null? new OrderServiceImpl():orderServiceImpl;
        return orderServiceImpl;
    }

    @Override
    public String getOrderDetailsById(int orderId) {
        Order order=ordersDao.findOrderById(orderId);
        String orderDetails= "-- Order Details --"+"\n"+
                "Order Id: "+order.getOrderId()+"\n"+
                "Buyer Id: "+order.getBuyerId()+"\n"+
                "Product Id: "+order.getProductId()+"\n"+
                "Ordered Quantity: "+order.getOrderedQuantity()+"\n"+
                "Ordered Date: "+ String.format("%1$tb %1$td, %1$tY",order.getOrderedDate())+"\n";
        return orderDetails;
    }

    @Override
    public String getOrdersForBuyer(int buyerId){
        String orderIdFormat = "| %1$6s\t| ";
        String productIdFormat = " %2$6s \t| ";
        String orderQuantityFormat = " %3$8s \t|";
        String dateFormat= " %4$tb %4$td, %4$tY | %n";
        String format = orderIdFormat.concat(productIdFormat).concat(orderQuantityFormat).concat(dateFormat);
        String line = new String(new char[56]).replace('\0','-');

        ArrayList<Order> orders=ordersDao.findOrderForBuyer(buyerId);
        String ordersDetails= "\n-- Order Details for Buyer "+buyerId+" --\n"+line+"\n"+
                "|  Order Id | Product Id|  Ordered Qty. | Ordered Date |\n";
        ordersDetails+=line+"\n";
        for(Order order:orders){
            if(order.getBuyerId()== buyerId)
                ordersDetails+=String.format(format,order.getOrderId(),order.getProductId(),order.getOrderedQuantity(),order.getOrderedDate());
        }
        ordersDetails+=line+"\n";
        return ordersDetails;
    }

    @Override
    public String getOrdersForSeller(int sellerId) {
        String productIdIdFormat = "| %1$6s\t| ";
        String quantityIdFormat = " %2$6s \t|%n";
//        String orderQuantityFormat = " %3$8s \t|";
//        String dateFormat= " %4$tb %4$td, %4$tY | %n";
        String format = productIdIdFormat.concat(quantityIdFormat);
        String line = new String(new char[25]).replace('\0','-');

        LinkedList<Product> products =productsDao.findProductsBySellerId(sellerId);
        HashMap<Integer,Integer> ordersForSeller= new HashMap<>();
        for(Product product:products){
            ordersForSeller.put(product.getProductId(),ordersDao.findOrderForSeller(product.getProductId()));
        }
        String ordersDetails= "\n-- Order Details for Seller "+sellerId+" --\n"+line+"\n"+
                "| Product Id|  Quantity |\n";
        ordersDetails+=line+"\n";
//        ordersDetails="";
//        String finalOrdersDetails = ordersDetails;
        Set<Map.Entry<Integer, Integer>> orders = ordersForSeller.entrySet();
        for(Map.Entry<Integer,Integer> order:orders){
            ordersDetails+=String.format(format,order.getKey(),order.getValue());
        }
        ordersDetails+=line+"\n";
        return ordersDetails;
    }
}
