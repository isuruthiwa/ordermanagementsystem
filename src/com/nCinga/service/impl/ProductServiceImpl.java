package com.nCinga.service.impl;

import com.nCinga.bo.Order;
import com.nCinga.bo.Product;
import com.nCinga.dao.ProductsDao;
import com.nCinga.service.ProductService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

public class ProductServiceImpl implements ProductService {
    private static ProductServiceImpl productService;
    private ProductsDao productsDao;

    private ProductServiceImpl() throws IOException {
        productsDao=ProductsDao.getInstance();
    }

    public static ProductServiceImpl getInstance() throws IOException {
        productService=productService==null? new ProductServiceImpl():productService;
        return productService;
    }

    @Override
    public void initStoreItems() {

    }

    @Override
    public String getProducts() {
        String productIdFormat = "| %1$6s\t| ";
        String sellerIdFormat = " %2$6s \t| ";
        String productNameFormat = " %3$8s \t|";
        String productPriceFormat= " %4$4s \t|";
        String availableQuantityFormat= " %5$9s \t|%n";
        String format = productIdFormat.concat(sellerIdFormat).concat(productNameFormat).
                concat(productPriceFormat).concat(availableQuantityFormat);
        String line = new String(new char[65]).replace('\0','-');

        LinkedList<Product> products=productsDao.getProducts();
        String productsDetails= "\n-- All Product Details --\n"+line+"\n"+
                "|Product Id | Seller Id |  Product Name | Price | Available Qty.|\n";
        productsDetails+=line+"\n";
        for(Product product:products){
            productsDetails+=String.format(format,product.getProductId(),product.getSellerId(),product.getProductName(),
                    product.getProductPrice(),product.getAvailableQuantity());
        }
        productsDetails+=line+"\n";
        return productsDetails;
    }

    @Override
    public String productsFromSeller(int sellerId) {
        String productIdFormat = "| %1$6s\t| ";
        String productNameFormat = " %2$8s \t|";
        String productPriceFormat= " %3$4s \t|";
        String availableQuantityFormat= " %4$9s \t|%n";
        String format = productIdFormat.concat(productNameFormat).
                concat(productPriceFormat).concat(availableQuantityFormat);
        String line = new String(new char[53]).replace('\0','-');

        LinkedList<Product> products=productsDao.findProductsBySellerId(sellerId);
        String productsDetails= "\n-- All Products From Seller --\n"+line+"\n"+
                "|Product Id |  Product Name | Price | Available Qty.|\n";
        productsDetails+=line+"\n";
        for(Product product:products){
            productsDetails+=String.format(format,product.getProductId(),product.getProductName(),
                    product.getProductPrice(),product.getAvailableQuantity());
        }
        productsDetails+=line+"\n";
        return productsDetails;
    }
}
