package com.nCinga.service.impl;

import com.nCinga.bo.Buyer;
import com.nCinga.dao.BuyersDao;
import com.nCinga.dao.OrdersDao;
import com.nCinga.dao.ProductsDao;
import com.nCinga.service.BuyerService;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BuyerServiceImpl implements BuyerService {
    private BuyersDao buyersDao;
    private OrdersDao ordersDao;
    private ProductsDao productsDao;

    private static BuyerServiceImpl buyerService;

    private BuyerServiceImpl() throws IOException {
        buyersDao= BuyersDao.getInstance();
        ordersDao=OrdersDao.getInstance();
        productsDao=ProductsDao.getInstance();
    }

    public static BuyerServiceImpl getInstance() throws IOException {
        buyerService = buyerService==null? new BuyerServiceImpl(): buyerService;
        return buyerService;
    }

    @Override
    public String getBuyerDetailsById(int buyerId) {
        Buyer buyer=buyersDao.findBuyerById(buyerId);
        String orderDetails= "-- Buyer Details --"+"\n"+
                "Buyer Id: "+buyer.getBuyerId()+"\n"+
                "Buyer Name: "+buyer.getBuyerName()+"\n";
        return orderDetails;
    }

    @Override
    public String placeAnOrder(int productId, int orderCount, int buyerId) {
        String orderDetails="";
        try {
            //Validation
            if(productsDao.orderProduct(productId,orderCount)) {
                String date=new SimpleDateFormat("dd/MM/yyyy").format(new Date());
                ordersDao.addNewOrder(buyerId, productId, orderCount, date);
                orderDetails="Order Success :: Order Details \n"+
                        "Buyer ID: "+buyerId+"\n"+
                        "Product ID: "+productId+"\n"+
                        "Order Quantity: "+orderCount+"\n"+
                        "Ordered Date: "+date+"\n\n"+
                        "Thank you for doing business with us";
            }
            else{
                orderDetails="Order Failed: Sorry, we are not able to provide your requirement\n";
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return orderDetails;
    }
}
