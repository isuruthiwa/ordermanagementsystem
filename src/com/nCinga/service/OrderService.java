package com.nCinga.service;

import com.nCinga.bo.Order;

import java.util.ArrayList;

public interface OrderService {
    String getOrderDetailsById(int orderId);
    String getOrdersForBuyer(int buyerId);
    String getOrdersForSeller(int sellerId);
}
