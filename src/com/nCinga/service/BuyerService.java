package com.nCinga.service;

import com.nCinga.bo.Buyer;

public interface BuyerService {
    String getBuyerDetailsById(int buyerId);
    String placeAnOrder(int productId, int orderCount, int buyerId);
}
