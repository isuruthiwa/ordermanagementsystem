package com.nCinga.service;

import com.nCinga.bo.Product;

import java.util.ArrayList;

public interface ProductService {
    void initStoreItems();
    String getProducts();
    String productsFromSeller(int sellerId);
}
